// CAROUSEL
$(".owl-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items: 3
    }
  }
});

// TABS MENU
$(document).ready(function () {
  $("#content section").hide();
  $("#tabs li:first").attr("id", "current");
  $("#content section:first").fadeIn();
  $("#tabs li a").click(function (e) {
    e.preventDefault();
    if ($(this).attr("id") == "current") {
      return;
    } else {
      $("#content section").hide();
      $("#tabs li").attr("id", "");
      $(this)
        .parent()
        .attr("id", "current");
      $($(this).attr("href")).fadeIn();
    }
  });
});

// MODAL
$('.modal-toggle').on('click', function (e) {
  e.preventDefault();
  $('.tabs-info__modal').toggleClass('tabs-info__modal-visible');
});

// MENU MOBILE
$(function () {
  $(".mobile").hide();
  $(".sub-menu").hide();
  $(".mobile-toggle").click(function () {
    $(this)
      .next(".mobile")
      .slideToggle();
  });
  $(document).click(function (e) {
    var target = e.target;
    if (
      $(target)
      .parents()
      .is(".mobile-toggle")
    ) {
      $(".mobile").hide();
    }
  });
  // SUB-MENU
  $(".title .itajai").on("click", function () {
    $(this)
      .next(".sub-menu")
      .slideToggle();
    $(".title .itajai")
      .next("sub-menu")
      .not($(this).next("sub-menu"))
      .hide();
  });
  // ARROW MENU
  $(".mobile-toggle").on("click", function (event) {
    $(".arrow-img").toggleClass("rotate");
    $(".arrow-img").toggleClass("rotate-reset");
  });
});