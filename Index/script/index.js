// CAROUSEL
$(".owl-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 3
    }
  }
});

$(document).ready(function() {
  /*******MENU TABS */
  $("#tabs_container .tab_contents").hide();

  $("#tabs_container .tab").click(function() {
    var target = $(this.rel);
    $(".tab_contents")
      .not(target)
      .slideUp();
    target.slideToggle();
    var $clickElement = $(this).hasClass("home-banner__menu-title--active");
    $(
      "#tabs_container > .tabs > li a.home-banner__menu-title--active"
    ).removeClass("home-banner__menu-title--active");

    $(this).addClass("home-banner__menu-title--active");
    if ($clickElement) {
      $(this).removeClass("home-banner__menu-title--active");
    }

    $(
      "#tabs_container > .tab_contents_container > div.tab_contents_active"
    ).removeClass("tab_contents_active");

    $(this.rel).addClass("tab_contents_active");
  });
  /**** Fechar menu click content */
  $(".home-content").on("click", function() {
    $("#tabs_container .tab_contents").slideUp();
    $("#tabs_container .tab").removeClass("home-banner__menu-title--active");
  });
});

// MENU MOBILE
$(function() {
  $(".mobile").hide();
  $(".sub-menu").hide();
  $(".mobile-toggle").click(function() {
    $(this)
      .next(".mobile")
      .slideToggle();
  });
  $(document).click(function(e) {
    var target = e.target;
    if (
      $(target)
        .parents()
        .is(".mobile-toggle")
    ) {
      $(".mobile").hide();
    }
  });
  // SUB-MENU
  $(".title .itajai").on("click", function() {
    $(this)
      .next(".sub-menu")
      .slideToggle();
    $(".title .itajai")
      .next("sub-menu")
      .not($(this).next("sub-menu"))
      .hide();
  });
  // ARROW MENU
  $(".mobile-toggle").on("click", function(event) {
    $(".arrow-img").toggleClass("rotate");
    $(".arrow-img").toggleClass("rotate-reset");
  });
});
